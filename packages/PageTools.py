


     ## -- --- --- --- --- --- ()   generate List
    ## 
    ##   generates and unordered list of links from a database query response
     ## -- --- --- --- --- --- --- --- -- ()

import site


def generateList (links):

    list = "<ul>\n"

    for row in links:
        list += f'\t<li><a href= "{row[2]}">{row[1]}</a></li>\n'
    list += "</ul>\n"

    return list
#...

    ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##




     ## -- --- --- --- --- --- ()   generate Site Menu
    ##
    ##   generates and site menu of links from a database query response
     ## -- --- --- --- --- --- --- --- -- ()

def generateSiteMenu (links):

    siteMenu = ""
    for row in links:
        siteMenu += f'\t<div><a href="{row[2]}">{row[1]}</a></div>\n'
    #...

    return siteMenu
#...

    ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##
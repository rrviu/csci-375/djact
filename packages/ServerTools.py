

     ## -- --- --- --- --- --- ()   parse URI
    ##
    ##   strips the relevant information pieces from a given URI
     ## -- --- --- --- --- --- --- --- -- ()

def parseURI (URI):

    ret = dict.fromkeys(['uri', '?', '.', '%', 'path', 'query', 'ext', 'db'])

    ret['uri'] = URI
    if ret['uri'] == '/': 
        ret['uri'] = "/index.html"

##  there is a database request
    ret['%'] = ret['uri'].rfind('%')
    if ret['%'] != -1:
        ret['db'] = ret['uri'][ret['%']+1: ]
        return ret

##  parse the query from the request
    ret['?'] = ret['uri'].rfind('?')
    ret['.'] = ret['uri'].rfind('.')
    if ret['?'] != -1:
        ret['query'] = ret['uri'][ret['?']+1: ]
    else: ret['?'] = len(ret['uri'])

##  parse the path and extension
    ret['path']  = ret['uri'][ :ret['?']]
    ret['ext']   = ret['uri'][ret['.'] : ret['?']]

    if ret['path'] == '/':
        ret['path'] = 'index.html'
    return ret
#...

    ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##

#from _typeshed import SupportsItemAccess
import os
from http.server import BaseHTTPRequestHandler, HTTPServer  ## python 3
#from http.cookies import SimpleCookie
from mimetypes import guess_type

## databse tools
# import sqlite3
# from sqlite3 import Error

## personal files
import packages.ServerTools as st
import packages.PageTools as pt
import sqliteAdmin.sqlAdmin as sqla


hostName = "localhost"
serverPort = 8000

dbfile = r"test2.db"
conn = sqla.cr_conn(dbfile)

## this is where the server looks for files requested by the browser
rootdir = str(os.path.dirname( os.path.abspath(__file__) )) + r"/html"
os.chdir(rootdir)  ## change to the html root directory



     ## -- --- --- --- --- --- ;;    server
    ##
    ##	
     ## -- --- --- --- --- --- --- --- -- ;;

class   server (BaseHTTPRequestHandler):


         ## -- --- --- --- --- --- ()   do _GET
        ##
        ##	
         ## -- --- --- --- --- --- --- --- -- ()

    def do_GET (self):


             ## -- --- --- --- --- --- ##   PARSE URI
            ##
            ##   
             ## -- --- --- --- --- --- --- --- -- ##
        
        pathVars = st.parseURI(self.path)
        # print(pathVars)

        if (
            pathVars['ext'] == '.php' 
            or pathVars['ext'] == '.bin' 
            or pathVars['ext'] == '.py'
            or pathVars['ext'] == '.ico'
            or (
                pathVars['db'] != None and (
                    pathVars['ext'] == '.html' or pathVars['ext'] == '.css'
                )
            )
        ):
            notImplemented(self)
            return
        #...

            ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##



             ## -- --- --- --- --- --- ##   HANDLE JS REQUEST
            ##
            ##   
             ## -- --- --- --- --- --- --- --- -- ##
        
        if pathVars['ext'] == '.js':
            filename = rootdir + pathVars['path']

        ##  file is accessible
            if(os.path.isfile(filename)):
          
                with open(filename,"rb") as f:
                    data=f.read()

                ##  send the HTTP status message to the browser, followed by the file contents
                    self.send_response(200)
                    self.send_header("Access-Control-Allow-Origin", "*")
                    self.send_header("Content-type", "text/html; charset=utf-8")
                    self.send_header("X-Content-Type-Options", "no-sniff")
                    self.end_headers()

                    self.wfile.write(data)
                #...
            #...

        #...

            ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##



             ## -- --- --- --- --- --- ##   HANDLE DB REQUEST
            ##
            ##   
             ## -- --- --- --- --- --- --- --- -- ##
        
        elif pathVars['db'] != None:
            links = sqla.sel_table(conn, pathVars['db'])
            siteMenu = pt.generateSiteMenu(links)

            self.send_response(200)
            self.send_header("Access-Control-Allow-Origin", "*")
            self.send_header("Content-type", "text/html; charset=utf-8")
            self.send_header("X-Content-Type-Options", "no-sniff")
            self.end_headers()
            self.wfile.write(siteMenu.encode("utf-8"))
        #...

            ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##



             ## -- --- --- --- --- --- ##   HANDLE URL
            ##
            ##   
             ## -- --- --- --- --- --- --- --- -- ##
        
        elif pathVars['path'] == "/" or pathVars['path'] == "/index.html": 
            if pathVars['path'] == "/":
                pathVars['path'] = pathVars['path'] + "index.html"
            filename = rootdir + pathVars['path']


        ##  file is accessible
            if(os.path.isfile(filename)):
                
                links = sqla.sel_table(conn, "menu")
                menu = pt.generateList(links)

                with open(filename,"r") as f:
                    data=f.read()

                ##  send the HTTP status message to the browser, followed by the file contents
                    self.send_response(200)
                    self.send_header("Access-Control-Allow-Origin", "*")
                    self.send_header("Content-type", "text/html; charset=utf-8")
                    self.send_header("X-Content-Type-Options", "no-sniff")
                    self.end_headers()

                    self.wfile.write((data.format(menu)).encode("utf-8"))
                #...
            #...
        #...

            ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##

        

             ## -- --- --- --- --- --- ##   HANDLE FILE NOT FOUND
            ##
            ##   
             ## -- --- --- --- --- --- --- --- -- ##
        
    ##  let the browser know we cannot open file
        else:
            self.send_response(404)
            self.send_header("Content-type","text/html")
            self.end_headers()
        #...

            ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##


    #...

        ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##



         ## -- --- --- --- --- --- ##   TO BE IMPLEMENTED
        ##
        ##   
         ## -- --- --- --- --- --- --- --- -- ##
    
    def do_HEAD (self):    notImplemented(self)
    def do_POST (self):    notImplemented(self)
    def do_PUT (self):     notImplemented(self)
    def do_POST (self):    notImplemented(self)
    def do_UPDATE (self):  notImplemented(self)
    def do_DELETE (self):  notImplemented(self)
    def do_CONNECT (self): notImplemented(self)
    def do_OPTIONS (self): notImplemented(self)
    def do_TRACE (self):   notImplemented(self)
    def do_PATCH (self):   notImplemented(self)

        ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##


#...

    ## - --- --- --- --- ,,, --- '''  pXq ''' --- ,,, --- --- --- --- - ##




    ##- --- --- --- --- --- --- --- --- --- --.###. --- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- -## --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- #########-- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- ##- ---##-- --- --- --- --- --- --- --- --- --- --- --- --- -##




     ## -- --- --- --- --- --- ()   not Implemented
    ##
    ##
     ## -- --- --- --- --- --- --- --- -- ()

def  notImplemented(self):

##  501 Not Implemented
    self.send_response(501)
    self.send_header("Content-type","text/html")
    self.end_headers()
#...


    ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##




    ##- --- --- --- --- --- --- --- --- --- #######.--- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- ##-###. --- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- ##- --##--- --- --- --- --- --- --- --- --- --- --- --- --- -##
    ##- --- --- --- --- --- --- --- --- --- ####### --- --- --- --- --- --- --- --- --- --- --- --- --- -##




     ## -- --- --- --- --- --- ()   RUN SERVER
    ##
    ##   only run the server if this module was called from the command line.
    ##   python makes the name different if this module is called by another module.
     ## -- --- --- --- --- --- --- --- -- ()

if  __name__ == "__main__":

    webServer = HTTPServer((hostName, serverPort), server)
    print(f"Server started http:{webServer.server_address}")

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass
    #...

    webServer.server_close()
    print("\nServer stopped.")
#...

    ## - --- --- --- --- ,,, --- ''' pXq ''' --- ,,, --- --- --- --- - ##